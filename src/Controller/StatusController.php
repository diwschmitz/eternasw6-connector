<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @copyright 2020 dasistweb GmbH (https://www.dasistweb.de)
 */
class StatusController extends AbstractController
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse(['success' => true]);
    }
}